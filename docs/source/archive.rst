.. _archive:


=================================================
Архивная версия документации
=================================================


.. toctree::
   :maxdepth: 1

   archive/preface
   archive/sphinx
   archive/rst-markup
   archive/sphinx-markup
   archive/git
   archive/rtd-gh
   archive/knownissues
   archive/faq
   archive/agreements
   archive/win-sphinx